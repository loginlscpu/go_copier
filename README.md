**HELP:**
```
Arguments:

  -h  --help                     Print help information
      posinitonal                action: list / cp
      posinitonal                Path on server 1 (if cp)
      posinitonal                Path on server 2 (if cp)
  -f  --from                     From server id
  -t  --to                       To server id
  -r  --recursive                Copy directory
  -p  --ping                     Check server by ping
  -s  --ssh                      Check server by ssh
  -c  --config                   Path to config file. default:
                                 ./configs/config.yaml
```

**USAGE EXAMPLES:**
```
program cp -r --from server1 --to server2 /root/dir /home/myuser/ --config ./configs/config.yaml
```
```
program  list --ping --ssh
```

**Program needs configuration file for working!!!**   
default config path =  ./configs/config.yaml  

**CONFIG EXAMPLE:**
```
servers:
  - id: "server1"
    ip: "192.168.50.11"
    login: "my_linux_user"
    key_path: "./configs/private.key"

  - id: "server2"
    ip: "192.168.50.11"
    login: "root"
    key_path: "./configs/key2.key"
```