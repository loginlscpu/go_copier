package config_worker

import (
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"log"
)

type Server struct {
	Id      string `yaml:"id"`
	Ip      string `yaml:"ip"`
	Longin  string `yaml:"login"`
	KeyPath string `yaml:"key_path"`
}

func (server Server) GetPrompt() string {
	return server.Longin + "@" + server.Ip
}

type Config struct {
	Servers []Server `yaml:"servers"`
}

func GetServers(pathToConfigFile string) []Server {
	file, _ := ioutil.ReadFile(pathToConfigFile)

	var config Config

	err := yaml.Unmarshal(file, &config)
	if err != nil {
		log.Fatal(err)
	}

	return config.Servers
}

func ValidateId(id string, servers []Server) bool {
	for _, server := range servers {
		if server.Id == id {
			return true
		}
	}
	return false
}
