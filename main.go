package main

import (
	"fmt"
	"github.com/akamensky/argparse"
	"os"
	"program/config_worker"
	"program/server_worker"
)

var pathToConfigFile = "./configs/config.yaml"

func main() {
	parser := argparse.NewParser("copier", "Help for copier")
	var action *string = parser.StringPositional(&argparse.Options{Required: false, Help: "action: list / cp"})
	var fromPath *string = parser.StringPositional(&argparse.Options{Required: false, Help: "Path on server 1 (if cp)"})
	var toPath *string = parser.StringPositional(&argparse.Options{Required: false, Help: "Path on server 2 (if cp)"})
	var fromServer *string = parser.String("f", "from", &argparse.Options{Required: false, Help: "From server id"})
	var toServer *string = parser.String("t", "to", &argparse.Options{Required: false, Help: "To server id"})
	var directory *bool = parser.Flag("r", "recursive", &argparse.Options{Required: false, Help: "Copy directory"})

	var ping *bool = parser.Flag("p", "ping", &argparse.Options{Required: false, Help: "Check server by ping"})
	var ssh *bool = parser.Flag("s", "ssh", &argparse.Options{Required: false, Help: "Check server by ssh"})

	var config *string = parser.String("c", "config", &argparse.Options{Required: false, Help: "Path to config file. default: ./configs/config.yaml"})

	err := parser.Parse(os.Args)

	if *config != "" {
		pathToConfigFile = *config
	}

	if *action != "list" && *action != "cp" {
		fmt.Println(parser.Usage(err))
	}

	if *action == "list" {
		PrintServers(*ping, *ssh)
	}

	if *action == "cp" {
		servers := config_worker.GetServers(pathToConfigFile)
		if !config_worker.ValidateId(*fromServer, servers) {
			fmt.Printf("Id %s is not valid!", *fromServer)
			return
		}
		if !config_worker.ValidateId(*fromServer, servers) {
			fmt.Printf("Id %s is not valid!", toServer)
			return
		}
		var serverFrom config_worker.Server
		var serverTo config_worker.Server

		for _, server := range servers {
			if server.Id == *fromServer {
				serverFrom = server
			}
			if server.Id == *toServer {
				serverTo = server
			}
		}

		server_worker.CopyTarget(serverFrom, serverTo, *fromPath, *toPath, *directory)
	}
}

func PrintServers(ping bool, ssh bool) {
	fmt.Println("SERVERS:")
	for _, server := range config_worker.GetServers(pathToConfigFile) {
		fmt.Printf("%-6s %-17s", server.Id, server.Ip)
		if ping {
			fmt.Printf("Ping: %-8t", server_worker.PingServer(server))
		}
		if ssh {
			fmt.Printf("SSH: %-8t", server_worker.SshServer(server))
		}
		fmt.Print("\n")
	}
}
