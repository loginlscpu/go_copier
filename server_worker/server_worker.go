package server_worker

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"program/config_worker"
	"strconv"
	"strings"
)

func PingServer(server config_worker.Server) bool {
	cmd := exec.Command("ping", "-c 1", server.Ip)
	if err := cmd.Run(); err != nil {
		return false
	}
	return true
}

func SshServer(server config_worker.Server) bool {
	stdout, _ := remoteExecuteWithTimeout("echo 100", server, 5)
	if strings.Trim(stdout, "\n") == "100" {
		return true
	}
	return false
}

func remoteExecute(command string, server config_worker.Server) (stdout, stderr string) {
	cmd := exec.Command("ssh", "-oStrictHostKeyChecking=no", "-i", server.KeyPath, server.GetPrompt(), command)
	var out bytes.Buffer
	cmd.Stdout = &out

	var er bytes.Buffer
	cmd.Stderr = &er

	if err := cmd.Start(); err != nil {
		fmt.Println(err)
	}

	if err := cmd.Wait(); err != nil {
		fmt.Println("ERROR!")
		fmt.Println("----")
		fmt.Println(cmd)
		//fmt.Println(err)
		fmt.Println(er.String())
		fmt.Println("----")
	}

	return out.String(), er.String()
}
func remoteExecuteWithTimeout(command string, server config_worker.Server, timeout int) (stdout, stderr string) {
	tmt := fmt.Sprintf("-oConnectTimeout=%d", timeout)
	cmd := exec.Command("ssh", "-oStrictHostKeyChecking=no", tmt, "-i", server.KeyPath, server.GetPrompt(), command)
	var out bytes.Buffer
	cmd.Stdout = &out

	var er bytes.Buffer
	cmd.Stderr = &er

	if err := cmd.Start(); err != nil {
		fmt.Println(err)
	}

	if err := cmd.Wait(); err != nil {
	}

	return out.String(), er.String()
}

func copyLocalToRemote(localPath string, remotePath string, server config_worker.Server) {
	cmd := exec.Command("scp", "-oStrictHostKeyChecking=no", "-i", server.KeyPath, localPath, remotePath)
	var er bytes.Buffer
	cmd.Stderr = &er
	if err := cmd.Run(); err != nil {
		fmt.Println(err)
		fmt.Println(er.String())
	}
}

func GetFreeSizeInBytes(server config_worker.Server, path string) int {
	stdout, _ := remoteExecute(fmt.Sprintf("df %s --output=avail | tail -n1", path), server)
	if i, err := strconv.Atoi(strings.Trim(stdout, "\n")); err == nil {
		return i
	} else {
		fmt.Println(err)
	}
	return -1
}

func GetTargetSize(server config_worker.Server, path string) int {
	stdout, _ := remoteExecute(fmt.Sprintf("du %s | awk '{print $1}' | tail -n 1", path), server)
	if i, err := strconv.Atoi(strings.Trim(stdout, "\n")); err == nil {
		return i
	} else {
		fmt.Println(err)
	}
	return -1
}

func CheckIfTargetExists(server config_worker.Server, path string, isDirectory bool) bool {
	flag := "-f"
	if isDirectory {
		flag = "-d"
	}
	stdout, _ := remoteExecute(fmt.Sprintf("test %s %s && echo YES", flag, path), server)
	return stdout == "YES\n"
}

func generateKeyPair(serverFrom config_worker.Server, serverTo config_worker.Server) string {
	_, err := os.Stat("./tmp")
	if os.IsNotExist(err) {
		err := os.Mkdir("./tmp", 0755)
		if err != nil {
			log.Fatal(err)
		}
	}
	fmt.Println("GENERATING KEYS")
	name := serverFrom.Id + serverTo.Id
	cmd := exec.Command("ssh-keygen", "-f", "./tmp/"+name)
	var er bytes.Buffer
	cmd.Stderr = &er
	if err := cmd.Run(); err != nil {
		fmt.Println("Keys already exist. Going to use old keys")
		fmt.Println(err)
		fmt.Println(er.String())
	}
	return name
}

func transferKeyPair(serverFrom config_worker.Server, serverTo config_worker.Server, name string) {
	fmt.Println("TRANSFERRING KEYS")
	var localPath string
	var remotePath string

	localPath = fmt.Sprintf("./tmp/%s", name)
	remotePath = fmt.Sprintf("%s:%s/.ssh/", serverFrom.GetPrompt(), GetHomePath(serverFrom))
	copyLocalToRemote(localPath, remotePath, serverFrom)

	localPath = fmt.Sprintf("./tmp/%s.pub", name)
	remotePath = fmt.Sprintf("%s:%s/.ssh/", serverFrom.GetPrompt(), GetHomePath(serverFrom))
	copyLocalToRemote(localPath, remotePath, serverFrom)

	publicKey, _ := ioutil.ReadFile(fmt.Sprintf("./tmp/%s.pub", name))
	remoteExecute(fmt.Sprintf("echo '%s' >> %s/.ssh/authorized_keys", string(publicKey), GetHomePath(serverTo)), serverTo)
}

func removeKeyPair(serverFrom config_worker.Server, serverTo config_worker.Server, name string) {
	fmt.Println("REMOVING KEYS")

	remoteExecute(fmt.Sprintf("rm %s/.ssh/%s", GetHomePath(serverFrom), name), serverFrom)
	publicKey, _ := ioutil.ReadFile(fmt.Sprintf("./tmp/%s.pub", name))
	remoteExecute(fmt.Sprintf("sed -i '\\,%s,d' %s/.ssh/authorized_keys", strings.Trim(string(publicKey), "\n"), GetHomePath(serverTo)), serverTo)

	cmd := exec.Command("rm", "./tmp/"+name, "./tmp/"+name+".pub")
	var er bytes.Buffer
	cmd.Stderr = &er
	if err := cmd.Run(); err != nil {
		fmt.Println(err)
		fmt.Println(er.String())
	}
}

func GetHomePath(server config_worker.Server) string {
	stdout, _ := remoteExecute("pwd", server)
	return strings.Trim(stdout, "\n")
}

func CopyTarget(serverFrom config_worker.Server, serverTo config_worker.Server, pathFrom string, pathTo string, isDirectory bool) {

	if !CheckIfTargetExists(serverFrom, pathFrom, isDirectory) {
		fmt.Printf("Target %s does not exist on server %s! directory: %t\n", pathFrom, serverFrom.Id, isDirectory)
		return
	}
	if !CheckIfTargetExists(serverTo, pathTo, true) {
		fmt.Printf("Target %s does not exist on server %s! directory: %t\n", pathTo, serverTo.Id, isDirectory)
		return
	}

	fileSize := GetTargetSize(serverFrom, pathFrom)
	freeSize := GetFreeSizeInBytes(serverTo, pathTo)
	if fileSize > freeSize {
		fmt.Println("Not enough space")
		return
	}

	keysName := generateKeyPair(serverFrom, serverTo)
	transferKeyPair(serverFrom, serverTo, keysName)

	flag := ""
	if isDirectory {
	}
	flag = " -r "

	fmt.Println("COPYING FILES")
	localKeyPath := GetHomePath(serverFrom) + "/.ssh/" + keysName
	scp := "scp" + flag + "-oStrictHostKeyChecking=no -i " + localKeyPath + " " + pathFrom + " " + serverTo.GetPrompt() + ":" + pathTo
	remoteExecute(scp, serverFrom)

	removeKeyPair(serverFrom, serverTo, keysName)
}
